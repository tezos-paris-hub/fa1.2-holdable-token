# FA1.2 Holdable template - Holdable fungible assets

import smartpy as sp

######################################################################################################
# Author: Sceme.io
# A template extension for FA1.2 to holdable tokens
#
# An extension to the FA1.2 standard token that allows tokens to be put on hold. 
# It guarantees a future transfer and makes the held tokens unavailable for transfer in the mean time.
# Holds are similar to escrows in that are firm and lead to final settlement.
#
# Note: Parameters 'lockPreimage' & 'recipient' are of type option and can be either 
# None or Some value 
######################################################################################################

# Core process of FA1.2 fungible tokens
class FA12_core(sp.Contract):
    def __init__(self, **extra_storage):
        self.init(balances = sp.big_map(tvalue = sp.TRecord(approvals = sp.TMap(sp.TAddress, sp.TNat), balance = sp.TNat)), 
                  totalSupply = 0,  
                  **extra_storage)

    @sp.entry_point
    def transfer(self, params):
        sp.set_type(params, sp.TRecord(from_ = sp.TAddress, to_ = sp.TAddress, value = sp.TNat).layout(("from_ as from", ("to_ as to", "value"))))
        sp.verify(self.is_administrator(sp.sender) |
            (~self.is_paused() &
                ((params.from_ == sp.sender) |
                 (self.data.balances[params.from_].approvals[sp.sender] >= params.value))), message = "Invalid parameters")
        self.addAddressIfNecessary(params.to_)
        sp.verify(self.data.balances[params.from_].balance >= params.value, message = "Unsufficient balance")
        self.data.balances[params.from_].balance = sp.as_nat(self.data.balances[params.from_].balance - params.value)
        self.data.balances[params.to_].balance += params.value
        sp.if (params.from_ != sp.sender) & (~self.is_administrator(sp.sender)):
            self.data.balances[params.from_].approvals[sp.sender] = sp.as_nat(self.data.balances[params.from_].approvals[sp.sender] - params.value)

    @sp.entry_point
    def approve(self, params):
        sp.set_type(params, sp.TRecord(spender = sp.TAddress, value = sp.TNat).layout(("spender", "value")))
        sp.verify(~self.is_paused(), message = "Contract is paused")
        alreadyApproved = self.data.balances[sp.sender].approvals.get(params.spender, 0)
        sp.verify((alreadyApproved == 0) | (params.value == 0), "UnsafeAllowanceChange")
        self.data.balances[sp.sender].approvals[params.spender] = params.value

    def addAddressIfNecessary(self, address):
        sp.if ~ self.data.balances.contains(address):
            self.data.balances[address] = sp.record(approvals = {}, balance = 0)
    
    # Return spendable balance
    @sp.view(sp.TNat)
    def getBalance(self, params):
        sp.result(self.data.balances[params].balance)

    @sp.view(sp.TNat)
    def getAllowance(self, params):
        sp.result(self.data.balances[params.owner].approvals[params.spender])

    @sp.view(sp.TNat)
    def getTotalSupply(self, params):
        sp.set_type(params, sp.TUnit)
        sp.result(self.data.totalSupply)

class FA12_mint_burn(FA12_core):
    @sp.entry_point
    def mint(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, value = sp.TNat))
        sp.verify(self.is_administrator(sp.sender), message = "Only administrator allowed")
        self.addAddressIfNecessary(params.address)
        self.data.balances[params.address].balance += params.value
        self.data.totalSupply += params.value

    @sp.entry_point
    def burn(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, value = sp.TNat))
        sp.verify(self.is_administrator(sp.sender), message = "Only administrator allowed")
        sp.verify(self.data.balances[params.address].balance >= params.value, message = "Insufficient balance")
        self.data.balances[params.address].balance = sp.as_nat(self.data.balances[params.address].balance - params.value)
        self.data.totalSupply = sp.as_nat(self.data.totalSupply - params.value)

class FA12_administrator(FA12_core):
    def is_administrator(self, sender):
        return sender == self.data.administrator

    @sp.entry_point
    def setAdministrator(self, params):
        sp.set_type(params, sp.TAddress)
        sp.verify(self.is_administrator(sp.sender), "Only administrator allowed")
        self.data.administrator = params

    @sp.view(sp.TAddress)
    def getAdministrator(self, params):
        sp.set_type(params, sp.TUnit)
        sp.result(self.data.administrator)

class FA12_pause(FA12_core):
    def is_paused(self):
        return self.data.paused

    @sp.entry_point
    def setPause(self, params):
        sp.set_type(params, sp.TBool)
        sp.verify(self.is_administrator(sp.sender), "Only administrator allowed")
        self.data.paused = params
   
# Holdable token extension        
class FA12_holdable(FA12_core):
    def is_held(self, holdId):
        return self.data.holds[holdId].status == self.HoldStatusCode.held
        
    def is_notary(self, sender, holdId):
        return sender == self.data.holds[holdId].notary

    def addHolderIfNecessary(self, sender):
        sp.if ~ self.data.accountHoldBalances.contains(sender):
            self.data.accountHoldBalances[sender] = sp.record(hold = 0)

    @sp.entry_point
    def hold(self, params):
        sp.set_type(params, sp.TRecord(amount = sp.TNat, notary = sp.TAddress, expiration = sp.TTimestamp, recipient = sp.TOption(t = sp.TAddress), lockHash = sp.TOption(t = sp.TBytes)))
        sp.verify(~self.is_paused(), message = "Contract is paused")
        sp.verify(params.amount > 0, message = "Invalid amount")
        sp.verify(self.data.balances[sp.sender].balance >= params.amount, message = "Insufficient balance")
        
        # Calculate key for hold entry in big_map
        holdId = sp.blake2b(sp.pack(params))
        sp.verify(~self.data.holds.contains(holdId), message = "Already referenced hold")
        
        # Process balance and hold movements
        self.data.balances[sp.sender].balance = sp.as_nat(self.data.balances[sp.sender].balance - params.amount)
        self.addHolderIfNecessary(sp.sender)
        self.data.accountHoldBalances[sp.sender].hold += params.amount
        self.data.totalSupplyOnHold += params.amount
        
        # Register hold entry
        self.data.holds[holdId] = sp.record(sender = sp.sender, recipient = params.recipient, notary = params.notary, amount = params.amount, expiration = params.expiration, status = self.HoldStatusCode.held, lockHash = params.lockHash)
        sp.if params.recipient.is_some():
            self.addAddressIfNecessary(params.recipient.open_some())
        
    @sp.entry_point
    def executeHold(self, params):
        sp.set_type(params, sp.TRecord(holdId = sp.TBytes, lockPreimage = sp.TOption(t = sp.TBytes), recipient = sp.TOption(t = sp.TAddress)))
        sp.verify(~self.is_paused(), message = "Contract is paused")
        sp.verify(self.is_held(params.holdId), message = "Not held anymore")
        sp.verify(self.is_notary(sp.sender, params.holdId), message = "Only notary allowed")
        
        # If lock key pre-image, then verify blake binary hash
        sp.if self.data.holds[params.holdId].lockHash.is_some():
            sp.verify(sp.sha256(params.lockPreimage.open_some()) == self.data.holds[params.holdId].lockHash.open_some(), message = 'Invalid lock pre-image key')
        
        # Update status and process balance movements
        self.data.holds[params.holdId].status = self.HoldStatusCode.executed
        self.data.totalSupplyOnHold = sp.as_nat(self.data.totalSupplyOnHold - self.data.holds[params.holdId].amount)
        self.data.accountHoldBalances[self.data.holds[params.holdId].sender].hold = sp.as_nat(self.data.accountHoldBalances[self.data.holds[params.holdId].sender].hold - self.data.holds[params.holdId].amount)
        sp.if self.data.holds[params.holdId].recipient.is_some():
            self.data.balances[self.data.holds[params.holdId].recipient.open_some()].balance += self.data.holds[params.holdId].amount
        sp.else: 
            self.addAddressIfNecessary(params.recipient.open_some())
            self.data.balances[params.recipient.open_some()].balance += self.data.holds[params.holdId].amount
        
    @sp.entry_point
    def releaseHold(self, params):
        sp.set_type(params, sp.TBytes)
        sp.verify(~self.is_paused(), message = "Contract is paused")
        sp.verify(self.is_held(params), message = "Not held anymore")
        
        # If caller is hold sender
        sp.if sp.sender == self.data.holds[params].sender:
            # Verify that expiration date is overdue
            sp.verify(sp.now >= self.data.holds[params].expiration, message = "Expiration date not reached yet")
        
        # Else check if caller is notary 
        sp.else:
            sp.verify(self.is_notary(sp.sender, params), message = "Only notary or sender allowed")
        
        # Update hold entry status and process balance movements
        self.data.holds[params].status = self.HoldStatusCode.released
        self.data.totalSupplyOnHold = sp.as_nat(self.data.totalSupplyOnHold - self.data.holds[params].amount)
        self.data.accountHoldBalances[self.data.holds[params].sender].hold = sp.as_nat(self.data.accountHoldBalances[self.data.holds[params].sender].hold - self.data.holds[params].amount)
        self.data.balances[self.data.holds[params].sender].balance += self.data.holds[params].amount
        
    @sp.view(sp.TNat)
    def getHoldBalance(self, params):
        sp.result(self.data.accountHoldBalances[params].hold)
    
    # Return gross balance i.e. spendable funds + funds currently held
    @sp.view(sp.TNat)
    def getGrossBalance(self, params):
        sp.result(self.data.balances[params].balance + self.data.accountHoldBalances[params].hold)

    @sp.view(sp.TNat)
    def getTotalSupplyOnHold(self, params):
        sp.set_type(params, sp.TUnit)
        sp.result(self.data.totalSupplyOnHold)
        
    @sp.view(sp.TString)
    def getHoldStatus(self, params):
        sp.set_type(params, sp.TBytes)
        sp.if self.data.holds.contains(params):
            sp.result(self.data.holds[params].status)
        sp.else:
            sp.result(self.HoldStatusCode.nonexistent)

class FA12(FA12_mint_burn, FA12_administrator, FA12_pause, FA12_holdable, FA12_core):
    def __init__(self, admin):
        self.HoldStatusCode = sp.record(held = "Held", executed = "Executed", released = "Released", nonexistent = "Non existent")
        FA12_core.__init__(self, 
                           paused = False, 
                           administrator = admin,
                           accountHoldBalances = sp.big_map(tvalue = sp.TRecord(hold = sp.TNat)),
                           holds = sp.big_map(tvalue = sp.TRecord(sender = sp.TAddress, recipient = sp.TOption(t = sp.TAddress), notary = sp.TAddress, amount = sp.TNat, expiration = sp.TTimestamp, status = sp.TString, lockHash = sp.TOption(t = sp.TBytes))), 
                           totalSupplyOnHold = 0)

class Viewer(sp.Contract):
    def __init__(self, t):
        self.init(last = sp.none)
        self.init_type(sp.TRecord(last = sp.TOption(t)))
    @sp.entry_point
    def target(self, params):
        self.data.last = sp.some(params)

if "templates" not in __name__:
    @sp.add_test(name = "FA12")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("FA1.2 Holdable template - Holdable fungible assets")

        scenario.table_of_contents()

        # sp.test_account generates ED25519 key-pairs deterministically:
        admin = sp.test_account("Administrator")
        alice = sp.test_account("Alice")
        bob   = sp.test_account("Robert")
        jack   = sp.test_account("Jack")
        notary   = sp.test_account("Notary")

        # Let's display the accounts:
        scenario.h1("Accounts")
        scenario.show([admin, alice, bob, jack, notary])

        scenario.h1("Contract")
        c1 = FA12(admin.address)

        scenario.h1("Entry points")
        scenario += c1
        scenario.h2("Admin mints a few coins")
        scenario += c1.mint(address = alice.address, value = 12).run(sender = admin)
        scenario += c1.mint(address = alice.address, value = 3).run(sender = admin)
        scenario += c1.mint(address = alice.address, value = 3).run(sender = admin)
        scenario.h2("Alice transfers to Bob")
        scenario += c1.transfer(from_ = alice.address, to_ = bob.address, value = 4).run(sender = alice)
        scenario.verify(c1.data.balances[alice.address].balance == 14)
        scenario.h2("Bob tries to transfer from Alice but he doesn't have her approval")
        scenario += c1.transfer(from_ = alice.address, to_ = bob.address, value = 4).run(sender = bob, valid = False)
        scenario.h2("Alice approves Bob and Bob transfers")
        scenario += c1.approve(spender = bob.address, value = 5).run(sender = alice)
        scenario += c1.transfer(from_ = alice.address, to_ = bob.address, value = 4).run(sender = bob)
        scenario.h2("Bob tries to over-transfer from Alice")
        scenario += c1.transfer(from_ = alice.address, to_ = bob.address, value = 4).run(sender = bob, valid = False)
        scenario.h2("Admin burns Bob token")
        scenario += c1.burn(address = bob.address, value = 1).run(sender = admin)
        scenario.verify(c1.data.balances[alice.address].balance == 10)
        scenario.h2("Alice tries to burn Bob token")
        scenario += c1.burn(address = bob.address, value = 1).run(sender = alice, valid = False)
        scenario.h2("Admin pauses the contract and Alice cannot transfer anymore")
        scenario += c1.setPause(True).run(sender = admin)
        scenario += c1.transfer(from_ = alice.address, to_ = bob.address, value = 4).run(sender = alice, valid = False)
        scenario.verify(c1.data.balances[alice.address].balance == 10)
        scenario.h2("Admin transfers while on pause")
        scenario += c1.transfer(from_ = alice.address, to_ = bob.address, value = 1).run(sender = admin)
        scenario.h2("Admin unpauses the contract and transfers are allowed")
        scenario += c1.setPause(False).run(sender = admin)
        scenario.verify(c1.data.balances[alice.address].balance == 9)
        scenario += c1.transfer(from_ = alice.address, to_ = bob.address, value = 1).run(sender = alice)
        scenario.h2("Bob tries to set an over holding transfer to Alice")
        scenario += c1.hold(amount = 10, notary = notary.address, expiration = sp.timestamp(1861340826), recipient = sp.some(alice.address), lockHash = sp.some(sp.bytes('0x537ef30020f832e317fb9e188e0145e48b4701a58d5e9e96b51844cda5eed8c1'))).run(sender = bob, valid = False)
        scenario.h2("Alice sets a holding transfer to Bob")
        scenario += c1.hold(amount = 4, notary = notary.address, expiration = sp.timestamp(1861340826), recipient = sp.some(bob.address), lockHash = sp.some(sp.bytes('0x537ef30020f832e317fb9e188e0145e48b4701a58d5e9e96b51844cda5eed8c1'))).run(sender = alice)
        scenario.verify(c1.data.accountHoldBalances[alice.address].hold == 4)
        scenario.h2("Bob tries to execute hold")
        scenario += c1.executeHold(holdId = sp.bytes('0x2b9f4ab90fb384c565b1205f075200143812a7ab1750b783223b5e5b836c4c9b'), lockPreimage = sp.some(sp.bytes('0x6c6f636b5f6b6579')), recipient= sp.none).run(sender = bob, valid = False)
        scenario.h2("Notary executes hold with wrong lock pre-image key")
        scenario += c1.executeHold(holdId = sp.bytes('0x2b9f4ab90fb384c565b1205f075200143812a7ab1750b783223b5e5b836c4c9b'), lockPreimage = sp.some(sp.bytes('0x005678')), recipient= sp.none).run(sender = notary, valid = False)
        scenario.h2("Notary executes Alice's hold")
        scenario += c1.executeHold(holdId = sp.bytes('0x2b9f4ab90fb384c565b1205f075200143812a7ab1750b783223b5e5b836c4c9b'), lockPreimage = sp.some(sp.bytes('0x6c6f636b5f6b6579')), recipient= sp.none).run(sender = notary)
        scenario.h2("Bob sets a holding transfer to Alice with empty lock key")
        scenario += c1.hold(amount = 5, notary = notary.address, expiration = sp.timestamp(1861340826), recipient = sp.some(alice.address), lockHash = sp.none).run(sender = bob)
        scenario.h2("Notary executes Bob's hold")
        scenario += c1.executeHold(holdId = sp.bytes('0x2ddfdf4b71990a61530d065c2f95692fa2d9204803583fbc2fa116b8356de3d5'), lockPreimage = sp.none, recipient= sp.none).run(sender = notary)
        scenario.h2("Alice sets a holding transfer with no recipient")
        scenario += c1.hold(amount = 2, notary = notary.address, expiration = sp.timestamp(1861340826), recipient = sp.none, lockHash = sp.some(sp.bytes('0x9578eed1c5a00bf599ee39e90ddd25f6e6c8aa8a2609f122094c86b13bd44f80'))).run(sender = alice)
        scenario.h2("Alice tries to release her hold")
        scenario += c1.releaseHold(sp.bytes('0xd1422090df9b5abb8a62ee41a19fb357cc19b8236993457c8df97d64f2308a28')).run(sender = alice, now = sp.timestamp(1608801041), valid = False)
        scenario.h2("Notary executes Alice's hold")
        scenario += c1.executeHold(holdId = sp.bytes('0xd1422090df9b5abb8a62ee41a19fb357cc19b8236993457c8df97d64f2308a28'), lockPreimage = sp.some(sp.bytes('0x6c6f636b5f6b657932')), recipient= sp.some(jack.address)).run(sender = notary)
        scenario.h2("Alice sets a holding transfer")
        scenario += c1.hold(amount = 3, notary = notary.address, expiration = sp.timestamp(1606845912), recipient = sp.none, lockHash = sp.some(sp.bytes('0x2ed3a2110071d34c092975a4cf802865b8bee36e2341c7e3c5278bb2de8abfc0'))).run(sender = alice)
        scenario.h2("Alice releases hold")
        scenario += c1.releaseHold(sp.bytes('0x50bfdbbe170096accb05f9ef058823a6aeea073dfdb52841c916a5b27e4bc087')).run(sender = alice, now = sp.timestamp(1608801041))
        scenario.h2("Alice sets a holding transfer")
        scenario += c1.hold(amount = 1, notary = notary.address, expiration = sp.timestamp(1861340826), recipient = sp.some(jack.address), lockHash = sp.some(sp.bytes('0xa176318c67b5e3e17be5904d3dd67fc886e7d175e690bb9d05872c1826314210'))).run(sender = alice)
        scenario.h2("Notary releases hold")
        scenario += c1.releaseHold(sp.bytes('0x9e1b3a2072cd03d8e95286b1cfaf184b8597c2e1d8538f67731b2d6ce8a2cd4d')).run(sender = notary, now = sp.timestamp(1608801041))
        scenario.h2("Alice sets a holding transfer")
        scenario += c1.hold(amount = 3, notary = notary.address, expiration = sp.timestamp(1861340826), recipient = sp.some(bob.address), lockHash = sp.none).run(sender = alice)

        scenario.verify(c1.data.totalSupply == 17)
        scenario.verify(c1.data.balances[alice.address].balance == 4)
        scenario.verify(c1.data.accountHoldBalances[alice.address].hold == 3)
        scenario.verify(c1.data.balances[bob.address].balance == 8)
        scenario.verify(c1.data.balances[jack.address].balance == 2)

        scenario.h1("Views")
        scenario.h2("Balance")
        view_balance = Viewer(sp.TNat)
        scenario += view_balance
        scenario += c1.getBalance((alice.address, view_balance.typed))
        scenario.verify_equal(view_balance.data.last, sp.some(4))
        
        scenario.h2("Balance in hold")
        view_balance = Viewer(sp.TNat)
        scenario += view_balance
        scenario += c1.getHoldBalance((alice.address, view_balance.typed))
        scenario.verify_equal(view_balance.data.last, sp.some(3))
        
        scenario.h2("Gross balance")
        view_balance = Viewer(sp.TNat)
        scenario += view_balance
        scenario += c1.getGrossBalance((alice.address, view_balance.typed))
        scenario.verify_equal(view_balance.data.last, sp.some(7))

        scenario.h2("Administrator")
        view_administrator = Viewer(sp.TAddress)
        scenario += view_administrator
        scenario += c1.getAdministrator((sp.unit, view_administrator.typed))
        scenario.verify_equal(view_administrator.data.last, sp.some(admin.address))

        scenario.h2("Total Supply")
        view_totalSupply = Viewer(sp.TNat)
        scenario += view_totalSupply
        scenario += c1.getTotalSupply((sp.unit, view_totalSupply.typed))
        scenario.verify_equal(view_totalSupply.data.last, sp.some(17))

        scenario.h2("Allowance")
        view_allowance = Viewer(sp.TNat)
        scenario += view_allowance
        scenario += c1.getAllowance((sp.record(owner = alice.address, spender = bob.address), view_allowance.typed))
        scenario.verify_equal(view_allowance.data.last, sp.some(1))

        scenario.h2("Hold status")
        view_holdStatus = Viewer(sp.TString)
        scenario += view_holdStatus
        scenario += c1.getHoldStatus((sp.bytes('0x50bfdbbe170096accb05f9ef058823a6aeea073dfdb52841c916a5b27e4bc087'), view_holdStatus.typed))
        scenario.verify_equal(view_holdStatus.data.last, sp.some('Released'))

        scenario.h2("Hold status for unreferenced entry")
        view_holdStatus = Viewer(sp.TString)
        scenario += view_holdStatus
        scenario += c1.getHoldStatus((sp.bytes('0x000000'), view_holdStatus.typed))
        scenario.verify_equal(view_holdStatus.data.last, sp.some('Non existent'))
